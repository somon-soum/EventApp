import { Component } from '@angular/core';
import { IonicPage, NavController } from 'ionic-angular';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { EventProvider } from '../../providers/event/event';

@IonicPage()
@Component({
  selector: 'page-event-create',
  templateUrl: 'event-create.html',
})
export class EventCreatePage {
  public formCreate: FormGroup;

  constructor(public navCtrl: NavController, public eventProvider: EventProvider,
    public formBuilder: FormBuilder) {
    this.formCreate = this.formBuilder.group({
      name: ['', Validators.required],
      cost: ['', Validators.required],
      price: ['', Validators.required],
      date: ['', Validators.required],
      location: ['', Validators.required],
      speaker: ['', Validators.required]
    });
  }

  createEvent(eventName: string, eventDate: Date, eventPrice: number, eventCost: number,
    eventLocation: string, eventSpeaker: string): void {
    this.eventProvider.createEvent(eventName, eventDate, eventPrice, eventCost,
      eventLocation, eventSpeaker)
      .then(newEvent => {
        this.navCtrl.setRoot('EventListPage');
      })
  }
}
