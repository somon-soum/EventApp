import { Component } from '@angular/core';
import { IonicPage } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-tabs',
  templateUrl: 'tabs.html',
})
export class TabsPage {
  tab1Root: string = 'EventListPage';
  tab2Root: string = 'ProfilePage';
  tab3Root: string = 'HomePage';
  tab4Root: string = 'AboutPage';

  constructor() { }
}
